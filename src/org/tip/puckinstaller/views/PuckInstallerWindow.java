/*
 * Copyright 2015-2024 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckinstaller.PuckInstallerException;
import org.tip.puckinstaller.model.InstallCriteria;
import org.tip.puckinstaller.util.BuildInformation;
import org.tip.puckinstaller.views.settings.SettingsPanel;

import fr.devinsy.util.cmdexec.CmdExec;
import fr.devinsy.util.cmdexec.CmdExecException;
import fr.devinsy.util.strings.StringList;

/**
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class PuckInstallerWindow extends JFrame
{
	public enum Step
	{
		WELCOME,
		SETTINGS,
		INSTALLING,
		INSTALLED
	}

	private static final long serialVersionUID = 5526792729445990591L;
	private static final Logger logger = LoggerFactory.getLogger(PuckInstallerWindow.class);

	private Step step = Step.WELCOME;
	private InstallCriteria criteria;

	private JPanel contentPane;
	private JButton btnFinish;
	private JButton btnBack;
	private JButton btnNext;
	private Component centerPanel;

	/**
	 * Create the frame.
	 */
	public PuckInstallerWindow()
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(PuckInstallerWindow.class.getResource("/org/tip/puckinstaller/favicon-16x16.jpg")));

		setTitle("PUCKInstaller");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 640, 475);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(this.contentPane);

		JPanel panel = new JPanel();
		this.contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut);

		this.btnBack = new JButton("Back");
		this.btnBack.setDefaultCapable(false);
		this.btnBack.setEnabled(false);
		this.btnBack.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Back button.
				switch (PuckInstallerWindow.this.step)
				{
					case SETTINGS:
						PuckInstallerWindow.this.step = Step.WELCOME;
						setPanel(new WelcomePanel());
						PuckInstallerWindow.this.btnBack.setEnabled(false);
						PuckInstallerWindow.this.btnNext.setEnabled(true);
						PuckInstallerWindow.this.btnNext.setText("Next");
						PuckInstallerWindow.this.btnFinish.setEnabled(false);
					break;

					case INSTALLING:
						PuckInstallerWindow.this.step = Step.SETTINGS;
						setPanel(new SettingsPanel());
						PuckInstallerWindow.this.btnBack.setEnabled(true);
						PuckInstallerWindow.this.btnNext.setEnabled(true);
						PuckInstallerWindow.this.btnNext.setText("Install");
						PuckInstallerWindow.this.btnFinish.setEnabled(false);
						pack();
					break;

					default:
						logger.debug("FOO ALERT");
				}
			}
		});
		panel.add(this.btnBack);

		Component horizontalGlue = Box.createHorizontalGlue();
		panel.add(horizontalGlue);

		this.btnNext = new JButton("Next");
		this.btnNext.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Next button.
				switch (PuckInstallerWindow.this.step)
				{
					case WELCOME:
						PuckInstallerWindow.this.step = Step.SETTINGS;
						setPanel(new SettingsPanel());
						PuckInstallerWindow.this.btnBack.setEnabled(true);
						PuckInstallerWindow.this.btnNext.setEnabled(true);
						PuckInstallerWindow.this.btnNext.setText("Install");
						PuckInstallerWindow.this.btnFinish.setEnabled(false);
					break;

					case SETTINGS:
						PuckInstallerWindow.this.criteria = ((SettingsPanel) PuckInstallerWindow.this.centerPanel).getCriteria();

						if (StringUtils.isBlank(PuckInstallerWindow.this.criteria.getPuckRepository()))
						{
							String title = "Error";
							String message = "Puck repository is required.";

							JOptionPane.showMessageDialog(PuckInstallerWindow.this, message, title, JOptionPane.ERROR_MESSAGE);
						}
						else if (PuckInstallerWindow.this.criteria.getInstallDirectory() == null)
						{
							String title = "Error";
							String message = "Install directory is required.";

							JOptionPane.showMessageDialog(PuckInstallerWindow.this, message, title, JOptionPane.ERROR_MESSAGE);
						}
						else if (StringUtils.isBlank(PuckInstallerWindow.this.criteria.getPuckPackage()))
						{
							String title = "Error";
							String message = "Missing Puck package selection.";

							JOptionPane.showMessageDialog(PuckInstallerWindow.this, message, title, JOptionPane.ERROR_MESSAGE);
						}
						else
						{
							boolean doAction;
							if (SystemUtils.IS_OS_MAC_OSX)
							{
								CmdExec command = new CmdExec("/usr/libexec/java_home -v 1.6");

								if (command.getExitValue() == 0)
								{
									logger.debug("stdout={}", command.getOutStream());
									PuckInstallerWindow.this.criteria.setSystemJreHome(new File(command.getOutStream().trim()));
								}
								else
								{
									PuckInstallerWindow.this.criteria.setSystemJreHome(null);
								}
								logger.debug("systemJreHome={}", PuckInstallerWindow.this.criteria.getSystemJreHome());

								if (PuckInstallerWindow.this.criteria.getSystemJreHome() == null)
								{
									String title = "Warning";
									StringList message = new StringList();
									message.appendln("PuckInstaller does not found Java 1.6 on your computer.");
									message.appendln("To ensure the best Puck functioning, you have to install Java 6.");
									message.appendln("Fortunately, it's easy, just install the following Apple update:");
									message.appendln("https://support.apple.com/kb/DL1572");
									message.appendln("(click on the middle button below to open it in your browser)");
									message.appendln("Then, come back to PuckInstaller.");
									String[] options = new String[] { "Use default Java", "Install Apple Java 6", "Cancel" };

									int choice = JOptionPane.showOptionDialog(PuckInstallerWindow.this, message.toString(), title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null,
											options, "Install Apple Java 6");

									logger.debug("Choice={}", choice);

									switch (choice)
									{
										case 0:
											doAction = true;
										break;

										case 1:
											doAction = false;
											logger.debug("browse desktop action supported={}", Desktop.getDesktop().isSupported(Desktop.Action.BROWSE));
											if (Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
											{
												try
												{
													Desktop.getDesktop().browse(new URI("https://support.apple.com/kb/DL1572"));
												}
												catch (IOException exception)
												{
													logger.debug("IOException browsing Apple update", exception);
												}
												catch (URISyntaxException exception)
												{
													logger.debug("URISyntaxException browsing Apple update", exception);
												}

												PuckInstallerWindow.this.btnBack.doClick();
											}
										break;

										case 2:
										default:
											doAction = false;
									}
								}
								else
								{
									doAction = true;
								}
							}
							else
							{
								doAction = true;
							}

							if (doAction)
							{
								PuckInstallerWindow.this.step = Step.INSTALLING;
								setPanel(new InstallingPanel(PuckInstallerWindow.this.criteria));
								PuckInstallerWindow.this.btnBack.setEnabled(false);
								PuckInstallerWindow.this.btnNext.setEnabled(false);
								PuckInstallerWindow.this.btnNext.setText("Next");
								PuckInstallerWindow.this.btnFinish.setEnabled(false);

								Thread t = new Thread()
								{
									@Override
									public void run()
									{
										try
										{
											((InstallingPanel) PuckInstallerWindow.this.centerPanel).install();

											PuckInstallerWindow.this.btnNext.setEnabled(true);
										}
										catch (PuckInstallerException exception)
										{
											PuckInstallerWindow.this.btnBack.setEnabled(true);
										}
									}
								};
								t.start();
							}
						}
					break;

					case INSTALLING:
						PuckInstallerWindow.this.step = Step.INSTALLED;
						setPanel(new InstalledPanel(PuckInstallerWindow.this.criteria));
						PuckInstallerWindow.this.btnBack.setEnabled(false);
						PuckInstallerWindow.this.btnNext.setEnabled(false);
						PuckInstallerWindow.this.btnNext.setText("Next");
						PuckInstallerWindow.this.btnFinish.setEnabled(true);
					break;

					default:
						logger.debug("FOO ALERT");
				}
			}
		});

		JLabel lblVersion = new JLabel("Version " + new BuildInformation().version());
		lblVersion.setEnabled(false);
		panel.add(lblVersion);

		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel.add(horizontalGlue_1);
		panel.add(this.btnNext);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_2);

		this.btnFinish = new JButton("Finish");
		this.btnFinish.setEnabled(false);
		this.btnFinish.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Finish button.
				switch (PuckInstallerWindow.this.step)
				{
					case INSTALLED:
						// Open a file browser window.
						if (SystemUtils.IS_OS_WINDOWS)
						{
							try
							{
								Runtime.getRuntime().exec("explorer \"" + PuckInstallerWindow.this.criteria.getInstallDirectory() + "\"");
							}
							catch (IOException exception)
							{
								logger.error(exception.getMessage(), exception);
							}
						}
						else if (SystemUtils.IS_OS_MAC_OSX)
						{
							try
							{
								{
									StringList command = new StringList();
									command.append("/usr/bin/osascript");
									command.append("-e");
									command.append("tell application \"Finder\" to open (POSIX File \"" + PuckInstallerWindow.this.criteria.getInstallDirectory() + "\")");
									CmdExec.run(command.toStringArray());
								}
								{
									StringList command = new StringList();
									command.append("/usr/bin/osascript");
									command.append("-e");
									command.append("tell application \"Finder\" to say \"Welcome to PUCK\"");
									CmdExec.run(command.toStringArray());
								}
							}
							catch (CmdExecException exception)
							{
								logger.error(exception.getMessage(), exception);
							}
						}

						//
						quit();
					break;

					default:
						logger.debug("FOO ALERT");
				}
			}
		});
		panel.add(this.btnFinish);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_1);

		this.centerPanel = new WelcomePanel();
		this.contentPane.add(this.centerPanel, BorderLayout.CENTER);

		// /////////////////////////////////////
		if (BuildInformation.isDefined())
		{
			setTitle("PUCKInstaller " + new BuildInformation().version());
		}
	}

	/**
	 * Close.
	 */
	public void close()
	{
		//
		String title = "Abort confirm request";
		String message = "Please, confirm you quit.";

		//
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.YES_NO_OPTION);

		//
		quit();
	}

	/**
	 * Quit.
	 */
	public void quit()
	{
		System.exit(0);
	}

	/**
	 * Sets the panel.
	 * 
	 * @param panel
	 *            the new panel
	 */
	public void setPanel(final Component panel)
	{
		PuckInstallerWindow.this.contentPane.remove(PuckInstallerWindow.this.centerPanel);
		PuckInstallerWindow.this.centerPanel = panel;
		PuckInstallerWindow.this.contentPane.add(panel, BorderLayout.CENTER);
		this.contentPane.validate();
	}
}
