/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.views.settings;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Collections;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckinstaller.model.InstallCriteria;
import org.tip.puckinstaller.model.NormalizedPuckPackageComparator;
import org.tip.puckinstaller.model.PuckInstallerManager;
import org.tip.puckinstaller.model.PuckRepository;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import fr.devinsy.util.strings.StringList;

/**
 * The Class SettingsPanel.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class SettingsPanel extends JPanel
{
	private static final long serialVersionUID = 6813353627595391145L;
	private static final Logger logger = LoggerFactory.getLogger(SettingsPanel.class);

	private JTextField txtfldInstallDirectory;
	private JSpinner spnrFreeMemory;
	private JSpinner spnrMemory;
	private JComboBox cmbxOperationSystem;
	private JComboBox cmbxPuckPackage;
	private JButton btnDefaultPuckRepository;
	private JButton btnDefaultJreRepository;
	private JButton btnDefaultInstallDirectory;
	private JButton btnAutoOperatingSystem;
	private JButton btnDefaultMemory;
	private JSpinner spnrPhysicalMemory;
	private JComboBox cmbxPuckRepository;
	private JComboBox cmbxJreRepository;
	private JButton btnJRERepositorySelect;

	/**
	 * Instantiates a new settings panel.
	 */
	public SettingsPanel()
	{
		setLayout(new BorderLayout(0, 0));

		JLabel lblSettings = new JLabel("Settings");
		lblSettings.setFont(new Font("Dialog", Font.BOLD, 32));
		add(lblSettings, BorderLayout.NORTH);

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		add(horizontalStrut_2, BorderLayout.WEST);

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		add(horizontalStrut_3, BorderLayout.EAST);

		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		JPanel sourcePanel = new JPanel();
		sourcePanel.setBorder(new TitledBorder(null, "Source", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(sourcePanel);
		sourcePanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("min(100dlu;default):grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, RowSpec.decode("4dlu:grow"), }));

		JLabel lblPuckPackageRepository = new JLabel("Puck repository:");
		sourcePanel.add(lblPuckPackageRepository, "2, 2, right, default");

		this.cmbxPuckRepository = new JComboBox()
		{
			private static final long serialVersionUID = 2954667544880878848L;

			/* (non-Javadoc)
			 * @see javax.swing.JComboBox#setSelectedItem(java.lang.Object)
			 */
			@Override
			public void setSelectedItem(final Object item)
			{
				super.setSelectedItem(item);
				ComboBoxEditor editor = getEditor();
				JTextField textField = (JTextField) editor.getEditorComponent();
				textField.setCaretPosition(0);
				logger.debug("selectedItem");
			}
		};
		this.cmbxPuckRepository.addItemListener(new ItemListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
			 */
			@Override
			public void itemStateChanged(final ItemEvent event)
			{
				if (event.getStateChange() == ItemEvent.SELECTED)
				{
					//
					logger.debug("itemStateChanged");
					updatePuckPackages();
				}
			}
		});
		sourcePanel.add(this.cmbxPuckRepository, "4, 2");
		this.cmbxPuckRepository
				.setModel(new DefaultComboBoxModel(new String[] { "http://kintip.net/index.php?option=com_jdownloads&view=category&catid=3", "http://www.devinsy.fr/Puck/Repository/" }));
		this.cmbxPuckRepository.setEditable(true);

		JButton btnPuckRepositorySelect = new JButton("…");
		btnPuckRepositorySelect.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Puck Repository Selector.
				File directory = DirectorySelector.showSelectorDialog(SettingsPanel.this, "Puck Repository Selector", null);

				if ((directory != null) && (directory.isDirectory()))
				{
					SettingsPanel.this.cmbxPuckRepository.setSelectedItem(directory.getAbsolutePath());
				}
			}
		});
		sourcePanel.add(btnPuckRepositorySelect, "6, 2");

		this.btnDefaultPuckRepository = new JButton("Default");
		this.btnDefaultPuckRepository.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				SettingsPanel.this.cmbxPuckRepository.setSelectedIndex(0);
			}
		});
		sourcePanel.add(this.btnDefaultPuckRepository, "8, 2");

		JLabel lblPuckPakage = new JLabel("Puck package:");
		sourcePanel.add(lblPuckPakage, "2, 4, right, default");

		this.cmbxPuckPackage = new JComboBox();
		sourcePanel.add(this.cmbxPuckPackage, "4, 4, fill, default");

		JLabel lblJreRepository = new JLabel("JRE repository:");
		sourcePanel.add(lblJreRepository, "2, 6, right, default");

		this.cmbxJreRepository = new JComboBox()
		{
			private static final long serialVersionUID = -8688011644070769286L;

			/* (non-Javadoc)
			 * @see javax.swing.JComboBox#setSelectedItem(java.lang.Object)
			 */
			@Override
			public void setSelectedItem(final Object item)
			{
				super.setSelectedItem(item);
				ComboBoxEditor editor = getEditor();
				JTextField textField = (JTextField) editor.getEditorComponent();
				textField.setCaretPosition(0);
				logger.debug("selectedItem");
			}
		};
		this.cmbxJreRepository.setModel(new DefaultComboBoxModel(new String[] { "http://kintip.net/index.php?option=com_jdownloads&view=category&catid=3", "http://www.devinsy.fr/Puck/Repository/",
				"None (use the default system JVM)" }));
		this.cmbxJreRepository.setEditable(true);
		sourcePanel.add(this.cmbxJreRepository, "4, 6, fill, default");

		this.btnJRERepositorySelect = new JButton("…");
		this.btnJRERepositorySelect.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// JRE Repository Selector.
				File directory = DirectorySelector.showSelectorDialog(SettingsPanel.this, "JRE Repository Selector", null);

				if ((directory != null) && (directory.isDirectory()))
				{
					SettingsPanel.this.cmbxJreRepository.setSelectedItem(directory.getAbsolutePath());
				}
			}
		});
		sourcePanel.add(this.btnJRERepositorySelect, "6, 6");

		this.btnDefaultJreRepository = new JButton("Default");
		this.btnDefaultJreRepository.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				SettingsPanel.this.cmbxJreRepository.setSelectedIndex(0);
			}
		});
		sourcePanel.add(this.btnDefaultJreRepository, "8, 6");

		JPanel targetPanel = new JPanel();
		targetPanel.setBorder(new TitledBorder(null, "Target", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(targetPanel);
		targetPanel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblInstallDirectory = new JLabel("Install directory:");
		targetPanel.add(lblInstallDirectory, "2, 2, right, default");

		this.txtfldInstallDirectory = new JTextField();
		targetPanel.add(this.txtfldInstallDirectory, "4, 2, fill, default");
		this.txtfldInstallDirectory.setColumns(10);

		JButton btnInstallDirectorySelect = new JButton("…");
		btnInstallDirectorySelect.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Install directory Selector.
				File directory = DirectorySelector.showSelectorDialog(SettingsPanel.this, "Install Directory Selector", null);

				if ((directory != null) && (directory.isDirectory()))
				{
					SettingsPanel.this.txtfldInstallDirectory.setText(directory.getAbsolutePath() + File.separatorChar);
				}
			}
		});
		targetPanel.add(btnInstallDirectorySelect, "6, 2");

		this.btnDefaultInstallDirectory = new JButton("Default");
		this.btnDefaultInstallDirectory.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Default Install Directory button.
				SettingsPanel.this.txtfldInstallDirectory.setText(InstallCriteria.getDefaultInstallDirectory().getAbsolutePath() + File.separatorChar);
			}
		});
		targetPanel.add(this.btnDefaultInstallDirectory, "8, 2");

		JLabel lblOperatingSystem = new JLabel("Operating System:");
		targetPanel.add(lblOperatingSystem, "2, 4, right, default");

		this.cmbxOperationSystem = new JComboBox();
		this.cmbxOperationSystem.setModel(new DefaultComboBoxModel(new String[] { "GNU/Linux - 32bits (i586)", "GNU/Linux - 64bits (amd64)", "Microsoft Windows - 32 bits",
				"Microsoft Windows - 64 bits", "OS/X - 32bits (i586)", "OS/X - 64bits (amd64)" }));
		targetPanel.add(this.cmbxOperationSystem, "4, 4, fill, default");

		this.btnAutoOperatingSystem = new JButton("Auto");
		this.btnAutoOperatingSystem.addActionListener(new ActionListener()
		{
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Button Auto Operating System.
				// SystemUtils.OS_ARCH;
				if (SystemUtils.IS_OS_LINUX)
				{
					if (PuckInstallerManager.is32bits())
					{
						SettingsPanel.this.cmbxOperationSystem.setSelectedIndex(0);
					}
					else
					{
						SettingsPanel.this.cmbxOperationSystem.setSelectedIndex(1);
					}
				}
				else if (SystemUtils.IS_OS_WINDOWS)
				{
					if (PuckInstallerManager.is32bits())
					{
						SettingsPanel.this.cmbxOperationSystem.setSelectedIndex(2);
					}
					else
					{
						SettingsPanel.this.cmbxOperationSystem.setSelectedIndex(3);
					}
				}
				else if (SystemUtils.IS_OS_MAC_OSX)
				{
					if (PuckInstallerManager.is32bits())
					{
						SettingsPanel.this.cmbxOperationSystem.setSelectedIndex(4);
					}
					else
					{
						SettingsPanel.this.cmbxOperationSystem.setSelectedIndex(5);
					}
				}
			}
		});
		targetPanel.add(this.btnAutoOperatingSystem, "8, 4");

		JLabel lblPhysicalMemory = new JLabel("Physical memory:");
		targetPanel.add(lblPhysicalMemory, "2, 6, right, default");

		this.spnrPhysicalMemory = new JSpinner();
		this.spnrPhysicalMemory.setEnabled(false);
		((JSpinner.DefaultEditor) this.spnrPhysicalMemory.getEditor()).getTextField().setHorizontalAlignment(JTextField.RIGHT);
		targetPanel.add(this.spnrPhysicalMemory, "4, 6");

		JLabel label = new JLabel("MB");
		targetPanel.add(label, "6, 6");

		JLabel lblFreeMemory = new JLabel("Free memory:");
		targetPanel.add(lblFreeMemory, "2, 8, right, default");

		this.spnrFreeMemory = new JSpinner();
		this.spnrFreeMemory.setEnabled(false);
		((JSpinner.DefaultEditor) this.spnrFreeMemory.getEditor()).getTextField().setHorizontalAlignment(JTextField.RIGHT);
		targetPanel.add(this.spnrFreeMemory, "4, 8");

		JLabel lblMb = new JLabel("MB");
		targetPanel.add(lblMb, "6, 8");

		JLabel lblMemory = new JLabel("Memory:");
		targetPanel.add(lblMemory, "2, 11, right, default");

		this.spnrMemory = new JSpinner();
		this.spnrMemory.setModel(new SpinnerNumberModel(new Integer(1024), new Integer(480), null, new Integer(1)));
		((JSpinner.DefaultEditor) this.spnrMemory.getEditor()).getTextField().setHorizontalAlignment(JTextField.RIGHT);
		targetPanel.add(this.spnrMemory, "4, 11");

		JLabel label_1 = new JLabel("MB");
		targetPanel.add(label_1, "6, 11");

		this.btnDefaultMemory = new JButton("Auto");
		this.btnDefaultMemory.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent event)
			{
				// Button Default Memory.
				SettingsPanel.this.spnrMemory.setValue(InstallCriteria.DEFAULT_MEMORY_MB);
			}
		});
		targetPanel.add(this.btnDefaultMemory, "8, 11");

		Component verticalStrut = Box.createVerticalStrut(20);
		panel_1.add(verticalStrut);

		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		Component horizontalStrut = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_1);

		// //////////////////////////////:
		setDefaultSetting();
	}

	/**
	 * Gets the criteria.
	 * 
	 * @return the criteria
	 */
	public InstallCriteria getCriteria()
	{
		InstallCriteria result;

		result = new InstallCriteria();

		result.setPuckRepository((String) this.cmbxPuckRepository.getSelectedItem());
		result.setPuckPackage((String) this.cmbxPuckPackage.getSelectedItem());
		String jreRepository = (String) this.cmbxJreRepository.getSelectedItem();
		if ((StringUtils.isBlank(jreRepository)) || (jreRepository.startsWith("None")))
		{
			result.setJreRepository(null);
		}
		else
		{
			result.setJreRepository((String) this.cmbxJreRepository.getSelectedItem());
		}
		if (StringUtils.isBlank(this.txtfldInstallDirectory.getText()))
		{
			result.setInstallDirectory(null);
		}
		else
		{
			result.setInstallDirectory(new File(this.txtfldInstallDirectory.getText()));
		}
		result.setOperatingSystem(InstallCriteria.OperatingSystem.values()[this.cmbxOperationSystem.getSelectedIndex()]);
		result.setMemory((Integer) this.spnrMemory.getValue());

		//
		return result;
	}

	/**
	 * Sets the default setting.
	 */
	private void setDefaultSetting()
	{
		updatePuckPackages();
		this.btnDefaultPuckRepository.doClick();
		this.btnDefaultJreRepository.doClick();
		this.btnDefaultInstallDirectory.doClick();
		this.spnrPhysicalMemory.setValue(PuckInstallerManager.getSystemPhysicalMemoryKB() / 1024);
		this.spnrFreeMemory.setValue(PuckInstallerManager.getAvailableSystemMemoryKB() / 1024);
		this.spnrMemory.setValue(InstallCriteria.DEFAULT_MEMORY_MB);
		this.btnAutoOperatingSystem.doClick();

		if (SystemUtils.IS_OS_MAC_OSX)
		{
			this.cmbxJreRepository.setSelectedIndex(2);
			this.cmbxJreRepository.setEnabled(false);
			this.btnJRERepositorySelect.setEnabled(false);
			this.btnDefaultJreRepository.setEnabled(false);
		}
	}

	/**
	 * Update puck packages.
	 */
	private void updatePuckPackages()
	{
		//
		String repositoryPath = (String) this.cmbxPuckRepository.getSelectedItem();
		logger.debug("repos=" + repositoryPath);

		//
		StringList packages = PuckRepository.loadPackages(repositoryPath);

		Collections.sort(packages, new NormalizedPuckPackageComparator());

		Collections.reverse(packages);
		this.cmbxPuckPackage.setModel(new DefaultComboBoxModel(packages.toArray()));
	}
}
