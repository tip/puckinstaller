/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller.model;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tools.ant.taskdefs.Expand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckinstaller.model.InstallCriteria.OperatingSystem;

import fr.devinsy.util.strings.StringList;
import fr.devinsy.util.strings.StringSet;

/**
 * The Class PuckRepository.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class PuckRepository
{
	private static final Logger logger = LoggerFactory.getLogger(PuckRepository.class);

	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	public static final String GNULINUX_32_JRE_FILENAME = "jre-6u45-linux-i586.zip";
	public static final String GNULINUX_64_JRE_FILENAME = "jre-6u45-linux-x64.zip";
	public static final String MSWINDOWS_32_JRE_FILENAME = "jre-6u45-msw-i586.zip";
	public static final String MSWINDOWS_64_JRE_FILENAME = "jre-6u45-msw-x64.zip";
	public static final String MACOSX_32_JRE_FILENAME = "jre-6uxx-osx-i386.zip";
	public static final String MACOSX_64_JRE_FILENAME = "jre-6uxx-osx-amd64.zip";

	/**
	 * Dowload jre.
	 * 
	 * @param jreRepository
	 *            the jre repository
	 * @param operatingSystem
	 *            the operating system
	 * @param targetDirectory
	 *            the target directory
	 * @return the file
	 * @throws MalformedURLException
	 *             the malformed URL exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static File dowloadJre(final String jreRepository, final OperatingSystem operatingSystem, final File targetDirectory) throws MalformedURLException, IOException
	{
		File result;

		String jreFileName = getJreFileName(operatingSystem);

		String source;
		if (jreRepository.contains("kintip.net"))
		{
			source = getKintipFileURL(jreRepository, jreFileName);
			result = new File(targetDirectory, jreFileName);
		}
		else
		{
			source = jreRepository + jreFileName;
			result = new File(targetDirectory, jreFileName);
		}

		result = download(source, result);

		//
		return result;
	}

	/**
	 * Dowload puck package.
	 * 
	 * @param puckRepository
	 *            the puck repository
	 * @param puckPackage
	 *            the puck package
	 * @param targetDirectory
	 *            the target directory
	 * @return the file
	 * @throws MalformedURLException
	 *             the malformed URL exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static File dowloadPuckPackage(final String puckRepository, final String puckPackage, final File targetDirectory) throws MalformedURLException, IOException
	{
		File result;

		String source;
		if (puckRepository.contains("kintip.net"))
		{
			source = getKintipFileURL(puckRepository, puckPackage) + ".zip";
			result = new File(targetDirectory, puckPackage.toLowerCase().replace("_", "-") + ".zip");
		}
		else
		{
			source = puckRepository + puckPackage + ".zip";
			result = new File(targetDirectory, puckPackage + ".zip");
		}

		logger.debug("[source={}]", source);
		logger.debug("[target={}]", result);

		result = download(source, result);

		//
		return result;
	}

	/**
	 * Download.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @return the file
	 * @throws MalformedURLException
	 *             the malformed URL exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static File download(final String source, final File target) throws MalformedURLException, IOException
	{
		File result;

		target.delete();

		if (source.startsWith("http"))
		{
			FileUtils.copyURLToFile(new URL(source), target);
		}
		else
		{
			FileUtils.copyFile(new File(source), target);
		}

		result = target;

		//
		return result;
	}

	/**
	 * Gets the jre file name.
	 * 
	 * @param source
	 *            the source
	 * @return the jre file name
	 */
	public static String getJreFileName(final InstallCriteria.OperatingSystem source)
	{
		String result;

		switch (source)
		{
			case GNULinux_32:
				result = GNULINUX_32_JRE_FILENAME;
			break;

			case GNULinux_64:
				result = GNULINUX_64_JRE_FILENAME;
			break;

			case MSWindows_32:
				result = MSWINDOWS_32_JRE_FILENAME;
			break;

			case MSWindows_64:
				result = MSWINDOWS_64_JRE_FILENAME;
			break;

			case OSX_32:
				result = MACOSX_32_JRE_FILENAME;
			break;

			case OSX_64:
				result = MACOSX_64_JRE_FILENAME;
			break;

			default:
				result = null;
		}

		//
		return result;
	}

	/**
	 * Gets the kintip file URL.
	 * 
	 * @param repositoryPath
	 *            the repository path
	 * @param fileName
	 *            the file name
	 * @return the kintip file URL
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String getKintipFileURL(final String repositoryPath, final String fileName) throws IOException
	{
		String result;

		//
		URL source = new URL(repositoryPath);

		URLConnection connection = source.openConnection();
		connection.setConnectTimeout(15000);
		connection.setReadTimeout(15000);

		byte[] data = IOUtils.toByteArray(connection);
		logger.info("Kintip catalog size=" + data.length);

		//
		StringList lines = toStringList(data, DEFAULT_CHARSET_NAME);

		// for (String line : lines)
		// {
		// logger.debug("LINE=" + line);
		// }

		String content = lines.toString();
		Pattern pattern = Pattern.compile("<a\\s+href=\"([^\"]+)\"[^>]*>\\s*" + fileName + "\\s*<");
		Matcher matcher = pattern.matcher(content);

		if (matcher.find())
		{
			logger.debug(matcher.groupCount() + " " + matcher.group(1));
			result = "http://kintip.net" + matcher.group(1);
		}
		else
		{
			logger.debug("File [" + fileName + "] not match.");
			throw new IOException("Filename not match.");
		}

		logger.debug("result=" + result);

		//
		return result;
	}

	/**
	 * Load packages.
	 * 
	 * @param repositoryPath
	 *            the repository path
	 * @return the string list
	 */
	public static StringList loadPackages(final String repositoryPath)
	{
		StringList result;

		logger.debug("repositoryPath=" + repositoryPath);

		//
		URLConnection connection = null;
		try
		{
			if (StringUtils.isBlank(repositoryPath))
			{
				result = new StringList();
			}
			else if (repositoryPath.startsWith("/"))
			{
				result = new StringList();

				for (File file : new File(repositoryPath).listFiles())
				{
					if ((file.isFile()) && (file.getName().matches("puck-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\w{0,9}\\.zip")))
					{
						result.add(FilenameUtils.removeExtension(file.getName()));
					}
				}
			}
			else if (repositoryPath.contains("kintip.net/"))
			{
				URL source = new URL(repositoryPath);

				connection = source.openConnection();
				connection.setConnectTimeout(15000);
				connection.setReadTimeout(15000);

				byte[] data = IOUtils.toByteArray(connection);
				logger.info("Kintip catalog size=" + data.length);

				//
				StringList lines = toStringList(data, DEFAULT_CHARSET_NAME);

				// for (String line : lines)
				// {
				// logger.debug("LINE=" + line);
				// }

				String content = lines.toString();
				Pattern pattern = Pattern.compile("<a\\s+href=[^>]+>([Pp]uck[-_]\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\w{0,9})<");
				
				Matcher matcher = pattern.matcher(content);

				StringSet names = new StringSet();
				while (matcher.find())
				{
					logger.debug(matcher.groupCount() + " " + matcher.group(1));
					names.add(matcher.group(1));
				}

				result = names.toStringList();

				logger.debug("result=" + result.toStringWithCommas());
			}
			else
			{
				URL source = new URL(repositoryPath);

				connection = source.openConnection();
				connection.setConnectTimeout(15000);
				connection.setReadTimeout(15000);

				byte[] data = IOUtils.toByteArray(connection);
				logger.info("Kinsources catalog size=" + data.length);

				//
				StringList lines = toStringList(data, DEFAULT_CHARSET_NAME);

				String content = lines.toString();
				Pattern pattern = Pattern.compile("(puck-\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\w{0,9}\\.zip)");
				Matcher matcher = pattern.matcher(content);

				StringSet names = new StringSet();
				while (matcher.find())
				{
					logger.debug(matcher.group(0));
					names.add(FilenameUtils.removeExtension(matcher.group(0)));
				}

				result = names.toStringList();

				logger.debug("result=" + result.toStringWithCommas());
			}
		}
		catch (MalformedURLException exception)
		{
			exception.printStackTrace();
			result = new StringList();
		}
		catch (FileNotFoundException exception)
		{
			exception.printStackTrace();
			result = new StringList();
		}
		catch (IOException exception)
		{
			exception.printStackTrace();
			result = new StringList();
		}
		finally
		{
			IOUtils.close(connection);
		}

		//
		return result;
	}

	/**
	 * To string list.
	 * 
	 * @param source
	 *            the source
	 * @param charsetName
	 *            the charset name
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList toStringList(final byte[] source, final String charsetName) throws IOException
	{
		StringList result;

		result = toStringList(new ByteArrayInputStream(source), charsetName);

		// Note: no need to close the input stream.

		//
		return result;
	}

	/**
	 * To string list.
	 * 
	 * @param source
	 *            the source
	 * @param charsetName
	 *            the charset name
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList toStringList(final InputStream source, final String charsetName) throws IOException
	{
		StringList result;

		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(source, charsetName));

			boolean ended = false;
			result = new StringList();
			while (!ended)
			{
				String line = in.readLine();
				if (line == null)
				{
					ended = true;
				}
				else
				{
					result.append(line);
				}
			}
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}

		//
		return result;
	}

	/**
	 * Unzip.
	 * 
	 * @param zipFile
	 *            the zip file
	 * @param targetDirectory
	 *            the target directory
	 */
	public static void unzip(final File zipFile, final File targetDirectory)
	{
		Expand unzipper = new Expand();
		unzipper.setSrc(zipFile);
		unzipper.setDest(targetDirectory);
		unzipper.execute();
	}
}
