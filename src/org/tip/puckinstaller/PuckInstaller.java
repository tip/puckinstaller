/*
 * Copyright 2015-2017 Christian Pierre MOMON, DEVINSY, TIP, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of PuckInstaller. This software (PuckInstaller) is a
 * computer program whose purpose is to install PUCK (Program for the Use and
 * Computation of Kinship data), an open interactive platform for archiving,
 * sharing, analyzing and comparing kinship data used in scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.tip.puckinstaller;

import java.awt.EventQueue;
import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tip.puckinstaller.util.GUIToolBox;
import org.tip.puckinstaller.views.PuckInstallerWindow;

/**
 * The Class PuckInstaller.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class PuckInstaller
{
	/** Lazy-loading with holder */
	private static class SingletonLoader
	{
		private static final PuckInstaller instance = new PuckInstaller();
	}

	private static final Logger logger = LoggerFactory.getLogger(PuckInstaller.class);

	private File puckInstallerHome;

	/**
	 * Launch the application.
	 */
	private PuckInstaller()
	{
		// Set PuckInstaller home directory.
		this.puckInstallerHome = new File(System.getProperty("user.home") + File.separator + ".puck");
		if (!this.puckInstallerHome.exists())
		{
			//
			this.puckInstallerHome.mkdir();
			logger.info("Puck home directory created: " + this.puckInstallerHome.getAbsolutePath());
		}

		// Remove BOLD on default font.
		UIManager.put("swing.boldMetal", Boolean.FALSE);

		// Set LookAndFeel.
		System.out.println("System lookAndFeel property:" + System.getProperty("swing.defaultlaf"));
		System.out.println("Available lookAndFeel: " + GUIToolBox.availableLookAndFeels().toString());
		System.out.println("System lookAndFeel: " + UIManager.getSystemLookAndFeelClassName());
		System.out.println("Current lookAndFeel: " + UIManager.getLookAndFeel().getName());

		if (!StringUtils.equals(UIManager.getSystemLookAndFeelClassName(), "javax.swing.plaf.metal.MetalLookAndFeel"))
		{
			try
			{
				System.out.println("Metal LAF setted and system LAF detected, try to set system LAF.");
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}
			catch (final Exception exception)
			{
				System.out.println("Failed to set the system LookAndFeel.");
			}
		}
		else if (GUIToolBox.availableLookAndFeels().toString().contains("GTK+"))
		{
			try
			{
				System.out.println("Metal LAF setted and GTK+ LAF detected, try to set GTK+ LAF.");
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			}
			catch (final Exception exception)
			{
				System.out.println("Failed to set the system LookAndFeel.");
			}
		}

		System.out.println("Activated lookAndFeel: " + UIManager.getLookAndFeel().getName());

		//
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
		{
			/**
             * 
             */
			@Override
			public void uncaughtException(final Thread thread, final Throwable exception)
			{
				String message;
				if (exception instanceof PuckInstallerException)
				{
					message = "Application error occured: " + exception.getMessage();
				}
				else if (exception instanceof OutOfMemoryError)
				{
					message = "Java ran out of memory!";
				}
				else
				{
					message = "Unexpected error occured: " + exception.getClass() + "(" + exception.getMessage() + ")";
				}

				JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
				logger.error(exception.getMessage(), exception);
			}
		});
	}

	/**
	 * Run.
	 */
	public void run()
	{
		new PuckInstallerWindow().setVisible(true);
	}

	/**
	 * Instance.
	 * 
	 * @return the puck installer
	 */
	public static PuckInstaller instance()
	{
		PuckInstaller result;

		result = SingletonLoader.instance;

		//
		return result;
	}

	/**
	 * Launch the application.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		//
		PropertyConfigurator.configure(PuckInstaller.class.getResource("/org/tip/puckinstaller/log4j.properties"));

		if ((args.length > 0) && (StringUtils.startsWithAny(args[0], "-h", "-help", "--help")))
		{
			System.out.println("puckinstaller [-h|-help|--help]");
		}
		else
		{
			EventQueue.invokeLater(new Runnable()
			{
				/**
				 * 
				 */
				@Override
				public void run()
				{
					try
					{
						PuckInstaller.instance().run();
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
				}
			});
		}
	}
}
